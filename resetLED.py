import RPi.GPIO as GPIO

playerpins = [7, 5]

cubepins = [
	[22, 2, 3, 4, 17, 18, 27],
	[8, 23, 24, 10, 9, 25, 11]
]

GPIO.setmode(GPIO.BCM)
for i in playerpins:
	GPIO.setup(i, GPIO.OUT)
	GPIO.output(i, 0)
for i in cubepins:
	for j in i:
		GPIO.setup(j, GPIO.OUT)
		GPIO.output(j, 0)
